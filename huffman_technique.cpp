//Source Tree Bit Bucket

#include<stdlib.h>
#include<iostream>
#include<conio.h>
#include<stdio.h>
#include<string.h>
#include<fstream>
using namespace std;
#define MAX 150
#define CHAR 130
#define LENGTH 200


class huffman_tree;
class node;
class treenode
{
    char character;
    int frequency;
    treenode *lc,*rc;

public:
    treenode(char ch,int f)
    {
        character=ch;
        frequency=f;
        lc=rc=NULL;
    }
    friend class huffman_tree;
    friend class node;
};

class node
{
    treenode *ptr;
    node *link;

public:
    node(treenode *n)
    {
        ptr=n;
        link=NULL;
    }

    friend class huffman_tree;
};




class huffman_tree
{
    treenode *root;
    int n,password;
    char code[CHAR][LENGTH],fname[15],al[CHAR],word[LENGTH];
    void create();
    void preorder(treenode*, int ,char* );
    node* insert(node*,treenode*);

public:
    huffman_tree()
    {
        root=NULL;
        create();
        cout<<root->character;
        n=0;
        preorder(root,0,word);
        cout<<n;
        password=4321;
    }

    void encode();
    void decode();
};

node* huffman_tree::insert(node *head,treenode *root)
{
	node *p,*q=NULL,*r;
	p=new node(root);
	if(head==NULL)
		return p;
	if(root->frequency<head->ptr->frequency)
	{
		p->link=head;
		return p;
	}
	r=head;
	while(r!=NULL&&root->frequency>=r->ptr->frequency)
	{
		q=r;
		r=r->link;
	}
	p->link=q->link;
	q->link=p;
	return head;
}

void huffman_tree::create()
{
    treenode *temp,*tree1,*tree2;
	node *head=NULL;
	int cnt=0,n,i,x;

	for(i=1;i<35;i++)
	{
		temp=new treenode((char)(i),i);
		head=insert(head,temp);
		cnt++;
	}

	for(int i=36;i<126;i++)
    {
        temp=new treenode((char)(i),i);
		head=insert(head,temp);
		cnt++;
    }

    n=cnt;
	for(i=1;i<n;i++)
	{
		tree1=head->ptr;
		tree2=head->link->ptr;
		head=head->link->link;
		x=tree1->frequency+tree2->frequency;
		temp=new treenode((char)'\0',x);
		temp->lc=tree1,temp->rc=tree2;
		head=insert(head,temp);
	}
	root=head->ptr;
}


void huffman_tree::preorder(treenode *root,int i,char word[])
{
	if(root!=NULL)
	{
			if(root->lc==NULL&&root->rc==NULL)
			{
				word[i]='\0';
				cout<<"\t"<<root->character<<" --- "<<word<<endl;
				al[n]=root->character;
				strcpy(code[n++],word);
			}
			word[i]='0';
			preorder(root->lc,i+1,word);
			word[i]='1';
			preorder(root->rc,i+1,word);
	}
}

void huffman_tree:: encode()
{
	int i=0,j,choice,res;
	char ch='a';
	ofstream fout;
     cout<<"\n\n\t\tENCRYPTION SYSTEM";
     cout<<"\n\n\tEnter the file u want to open: ";
     cin>>fname;
     strcat(fname,".txt");
     fout.open(fname);
	fflush(stdin);
	cout<<"\n\n\tEnter message you want to encode : ";
	while(ch!='#')
     {
         ch=getch();
         cout<<ch;
        if(ch==13)
        {
            fout<<"\n";
            cout<<"\n";
        }
        else if(ch==32)
            fout<<" ";
        else if(ch==9)
            fout<<"\t";
        else
            fout.put(ch);
    }
    fout.close();
    ifstream fin;

    fin.open(fname);
     ofstream fo;
     fo.open("encoded.txt");
	ch=fin.get();

	while(ch!='#')
	{
		for(j=0;al[j]!=ch&&j<n;j++);
            if(j<n)
            {
                fo<<code[j];
            }
		ch=fin.get();

	}
	fo.close();
    fin.close();
    cout<<"\n\n\tYour message has been successfully Encoded!";

}


void huffman_tree::decode()
{
	int password1,flag=0,i=0;
     char ch,che;
	treenode *q;

     ifstream fi;
     cout<<"\n\n\tDecode your message here!";
	fi.open("encoded.txt");
	fi.clear();
	q=root;
	int cnt=3;
	do
	{
     cout<<"\n\n\tEnter the password:";
	cin>>password1;

	if(password==password1)
	{
	     flag=1;
	     cout<<"\n\n\tpassword matched!";
	     break;
	}
	else
	{
	     cnt--;
          cout<<"\n\n\toops wrong password!\n\n\tYou have "<<cnt<<" chances left!";
	}
	}while(cnt>0);
	if(flag==1)
	{
	cout<<"\n\n\tDecoded message is:";
	while((che=fi.get())!=EOF)
	{
		if(che=='0')
			q=q->lc;
		else
			q=q->rc;
		if(q->lc==NULL&&q->rc==NULL)
		{
		    ch=q->character;
                		    cout<<ch;
		    q=root;
		}
	}
	}
	else return;
	fi.close();
}


int main()
{
    huffman_tree h;
    h.encode();
    h.decode();
    return 0;
}
